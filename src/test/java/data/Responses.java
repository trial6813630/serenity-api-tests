package data;

public enum Responses {
    OK_SUCCESS(200, false, null),
    UNAUTHORIZED(401, true, "Not authenticated"),
    NOT_FOUND(404, true, "Not found");

    private int statusCode;
    private boolean isError;
    private String message;

    private Responses(int statusCode, boolean isError, String message) {
        this.statusCode = statusCode;
        this.isError = isError;
        this.message = message;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public boolean getIsError() {
        return isError;
    }

    public String getMessage() {
        return message;
    }
}
