package config;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

public class ProductApiConfig {
    public void init() {
         if (SerenityRest.getDefaultRequestSpecification() == null) {
             RestAssured.requestSpecification = new RequestSpecBuilder()
                     .setBaseUri("https://waarkoop-server.herokuapp.com")
                     .setBasePath("/api/v1")
                     .addFilter(new RequestLoggingFilter())
                     .addFilter(new ResponseLoggingFilter())
                     .build();
             SerenityRest.setDefaultRequestSpecification(RestAssured.requestSpecification);
         }
    }
}
