package common;

import config.ProductApiConfig;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import config.ProductApiEndpoints;

public class ProductApi extends ProductApiConfig {
    @Step("Execute GET request with search parameter `{0}`")
    public void getRequestWithParam(String pathParam) {
        SerenityRest
                .given()
                    .pathParam("product", pathParam)
                .when()
                    .get(ProductApiEndpoints.SEARCH_PRODUCT);
    }
}
