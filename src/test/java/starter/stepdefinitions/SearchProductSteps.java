package starter.stepdefinitions;

import common.ProductApi;
import static data.Responses. *;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.Matchers.*;

public class SearchProductSteps {
    String searchParam = null;

    @Steps
    public ProductApi productApi;

    @Before
    public void beforeScenario() {
        productApi.init();
    }

    @Given("I set {string} as product for searching")
    public void givenISetAsProductForSearching(String arg0) {
        searchParam = arg0;
    }

    @Given("I do not set any product for searching")
    public void givenIDoNotSetAnyProductForSearching() {
        searchParam = "";
    }

    @When("I execute GET request for searching product")
    public void whenIExecuteGetRequestForSearchingProduct() {
        productApi.getRequestWithParam(searchParam);
    }

    @Then("I should see the success response with data")
    public void thenIShouldSeeTheSuccessResponseWithData() {
        restAssuredThat(response -> response.statusCode(OK_SUCCESS.getStatusCode())
                .body("size()", not(0)));
    }

    @And("I should see {string} product is returned in the title")
    public void thenIShouldSeeProductIsReturnedInTheTitle(String arg0) {
        restAssuredThat(response -> response.body("title", everyItem(containsStringIgnoringCase(arg0))));
    }

    @Then("I should see that the requested resource not found")
    public void thenIShouldSeeThatTheRequestedResourceNotFound() {
        restAssuredThat(response -> response.statusCode(NOT_FOUND.getStatusCode())
                .body("detail.message", equalTo(NOT_FOUND.getMessage())));
    }

    @Then("I should see that the requested resource has incorrect URL")
    public void thenIShouldSeeThatTheRequestedResourceHasIncorrectUrl() {
        restAssuredThat(response -> response.statusCode(UNAUTHORIZED.getStatusCode())
                .body("detail", equalTo(UNAUTHORIZED.getMessage())));
    }
}
