Feature: Checking GET endpoint for searching by product name

  @positive
  Scenario Outline: Search by existing product
    Given I set "<name>" as product for searching
    When I execute GET request for searching product
    Then I should see the success response with data
    And I should see "<name>" product is returned in the title

    Examples:
      | name |
      | apple |
      | orange |
      | cola |
      | pasta |

  @negative
  Scenario Outline: Search by invalid product
    Given I set "<name>" as product for searching
    When I execute GET request for searching product
    Then I should see that the requested resource not found

    Examples:
      | name  |
      | car |
      | 'apple' |
      | ora nge |
      | Cola |
      | PASTA |

  @negative
  Scenario: Search without any product
    Given I do not set any product for searching
    When I execute GET request for searching product
    Then I should see that the requested resource has incorrect URL