# Test project with Serenity, Rest Assured and Cucumber
This project was done as test task<br />

## Test scenarios
Endpoint: GET https://waarkoop-server.herokuapp.com/api/v1/search/demo/{product}. <br />
Products: "orange", "apple", "pasta", "cola". <br />
Positive and negative scenarios are written. <br />

### The project structure
```
src
  + main
  + test
    + java
      + common                   Contains file with GET API setup
      + config                   Contains files with base test configuration and endpoints
      + data                     Contains file with Repsonses description 
      + starter                  
        + stepdefinitions        Contains file with steps according to the feature file   
    + resources
      + features                  
            + search                 Contains feature file with scenarios for GET requests 
```

## Before running tests
Install JAVA <br />
Install Maven <br />
Link to GITlab: https://gitlab.com/trial6813630/serenity-api-tests <br />

## Executing tests
Run all tests: <br />
`mvn clean verify`<br />
The standard test report with results will be located in the `target/site/serenity` directory. <br />
In order to open test report run the following script: <br />
`open ./target/site/serenity/index.html` (for Mac OS) <br />
`<BROWSER_NAME> ./target/site/serenity/index.html` (for Linux) <br />

## Gitlab CI/CD
Gitlab CICD is able to execute testcases. <br />

### Explanation
* Created `common` folder contains: <br />
`ProductApi.java` with `getRequestWithParam` method for executing GET request with passed parameter as product type. <br />
* Created `config` folder with two files: <br />
`ProductApiConfig.java` with basic config for request called via `init` method <br />
`ProductApiEndpoints.java` class for storing endpoints of Product API <br />
* Created `data` folder contains: <br />
`Respnonses.java` as enam type with three basic models of response: 200, 401 and 404. <br />
* Changed `SearchProductSteps.java` according to the new version of feature file. <br />
* `search_products_by_name.feature` renamed and changed feature file with basic positive and negative scenarios for GET request.
